
counter([], 0).
counter([Head|Tail], N) :-
	number(Head),
	counter(Tail,N1),
	N is N1 + 1.

counter([_|Tail], Count) :-
	counter(Tail, Count).