import scala.collection.mutable._

class ControlFunction {
  def respond(input: String): String = {
    val (opcode, paramMap) = CommandParser(input)
    if (opcode == "React") {
      val viewString = paramMap("view")

      val g = new UndirectedGraph(1)

      var myMap: Map[String, UndirectedGraph#Node] = Map()

      val view = View(viewString, g, myMap)
      val listA = view.musu()
      listA foreach { case (value) => "" }

      view.offsetToNearest('B') match {
        case Some(offset) =>

          val (start, target) = (myMap((480).toString), myMap((480 + offset.x + offset.y * 31).toString))
          val dijkstra = new Dijkstra[g.type](g)
          dijkstra.stopCondition = (S, D, P) => !S.contains(target)
          val (distance, path) = dijkstra.compute(start, target)

          var lastStep = 0;
          lastStep = printResult(start, target, distance, path)
          "Move(direction=" + (lastStep % 31 - 16) + ":" + ((lastStep / 31) - 15) + ")|Status(text=VejuosiuZveri)"
          case None =>
           view.offsetToNearest('P') match {

            case Some(offset) =>

              val (start2, target2) = (myMap((480).toString), myMap((480 + offset.x + offset.y * 31).toString))
              val dijkstra = new Dijkstra[g.type](g)
              dijkstra.stopCondition = (S, D, P) => !S.contains(target2)
              val (distance2, path2) = dijkstra.compute(start2, target2)
              var lastStep = printResult(start2, target2, distance2, path2)
              "Move(direction=" + (lastStep % 31 - 16) + ":" + ((lastStep / 31) - 15) + ")|Status(text=RenkuGeles)"
            case None =>
              
             view.offsetToNearest('b') match {
                case Some(offset) =>
                
                  val (start3, target3) = (myMap((480).toString), myMap((480 + offset.x + offset.y * 31).toString))
                  val dijkstra = new Dijkstra[g.type](g)
                  dijkstra.stopCondition = (S, D, P) => !S.contains(target3)
                  val (distance3, path3) = dijkstra.compute(start3, target3)
                  var lastStep = printResult(start3, target3, distance3, path3)
                  "Move(direction=" + (lastStep % 31 - 16) + ":" + ((lastStep / 31) - 15) + ")|Status(text=RenkuGeles)"
               
                
                case None =>              
                 view.offsetToNearest('W') match {
                    case Some(offset) =>
                    "Move(direction=" + offset.x * (-1) + ":" + offset.y * (-1) + ")|Status(text=BeguNuoSienos)"
                 }
             }
           }
      }
    } else ""
  }

  def printResult(start: UndirectedGraph#Node, target: UndirectedGraph#Node,
                  distance: Map[UndirectedGraph#Node, Int], path: Map[UndirectedGraph#Node, UndirectedGraph#Node]): Int = {
    var shortest = List(target)
    while (shortest.head != start) {
      shortest ::= path(shortest.head)
    }
    shortest.toList(1).toString.toInt
  }
}

object CommandParser {
  def apply(command: String) = {
      def splitParam(param: String) = {
        val segments = param.split('=')
        if (segments.length != 2)
          throw new IllegalStateException("invalid key/value pair: " + param)
        (segments(0), segments(1))
      }

    val segments = command.split('(')
    if (segments.length != 2)
      throw new IllegalStateException("invalid command: " + command)

    val params = segments(1).dropRight(1).split(',')
    val keyValuePairs = params.map(splitParam).toMap
    (segments(0), keyValuePairs)
  }
}

class ControlFunctionFactory {
  def create = new ControlFunction().respond _
}

abstract class Graph {
  type Edge <: IEdge
  type Node <: INode
  abstract class INode {
  }
  abstract class IEdge {
    def a: UndirectedGraph#Node
    def b: UndirectedGraph#Node
    def opposite(n: UndirectedGraph#Node): Option[UndirectedGraph#Node]
  }
  def nodes: List[UndirectedGraph#Node]
  def edges: List[UndirectedGraph#Edge with Weight]
  def addNode: UndirectedGraph#Node

  trait Weight {
    var weight = 1
    def getWeight = weight
    def setWeight(weight: Int): Unit = {
      this.weight = weight
    }
  }
}

class UndirectedGraph(defaultWeight: Int) extends Graph {
  class EdgeImpl(one: UndirectedGraph#Node, other: UndirectedGraph#Node) extends IEdge {
    def a = one
    def b = other
    def opposite(n: UndirectedGraph#Node): Option[UndirectedGraph#Node] =
      if (n == a) Some(b)
      else if (n == b) Some(a)
      else None
  }

  class NodeImpl extends INode {
    this: Node =>
    def connectWith(node: UndirectedGraph#Node): UndirectedGraph#Edge with Weight = {
      val edge = newEdge(this, node)
      edges = edge :: edges;
      edge
    }
    override def toString: String =
      (nodes.length - nodes.indexWhere(this == _)).toString()
  }

  type Node = NodeImpl
  type Edge = EdgeImpl

  protected def newNode: UndirectedGraph#Node = new NodeImpl
  protected def newEdge(one: UndirectedGraph#Node, other: UndirectedGraph#Node): UndirectedGraph#Edge with Weight = new EdgeImpl(one, other) with Weight

  var nodes: List[UndirectedGraph#Node] = Nil
  var edges: List[UndirectedGraph#Edge with Weight] = Nil

  def addNode: UndirectedGraph#Node = {
    val node = newNode
    nodes = node :: nodes
    node
  }
}
class Dijkstra[G <: UndirectedGraph](graph: G) {
  type Node = UndirectedGraph#Node
  type Edge = UndirectedGraph#Edge
  /**
   * StopCondition provides a way to terminate the algorithm at a certain
   * point, e.g.: When target becomes settled.
   */
  type StopCondition = (Set[Node], Map[Node, Int], Map[Node, Node]) => Boolean

  /**
   * By default the Dijkstra algorithm processes all nodes reachable from
   * start given to compute().
   */
  val defaultStopCondition: StopCondition = (_, _, _) => true
  var stopCondition = defaultStopCondition

  def compute(start: UndirectedGraph#Node, target: UndirectedGraph#Node): (Map[UndirectedGraph#Node, Int], Map[UndirectedGraph#Node, UndirectedGraph#Node]) = {
    var queue: Set[UndirectedGraph#Node] = new HashSet()
    var settled: Set[UndirectedGraph#Node] = new HashSet()
    var distance: Map[UndirectedGraph#Node, Int] = new HashMap()
    var path: Map[UndirectedGraph#Node, UndirectedGraph#Node] = new HashMap()
    queue += start
    distance(start) = 0

    while (!queue.isEmpty && stopCondition(settled, distance, path)) {
      val u = extractMinimum(queue, distance)
      settled += u
      relaxNeighbors(u, queue, settled, distance, path)
    }
    return (distance, path)
  }

  /**
   * Finds element of Q with minimum value in D, removes it
   * from Q and returns it.
   */
  protected def extractMinimum[T](Q: Set[T], D: Map[T, Int]): T = {
    var u = Q.head
    Q.foreach((node) => if (D(u) > D(node)) u = node)
    Q -= u
    return u;
  }

  /**
   * For all nodes v not in S, neighbors of
   * u}: Updates shortest distances and paths, if shorter than
   * the previous value.
   */
  protected def relaxNeighbors(u: UndirectedGraph#Node, Q: Set[UndirectedGraph#Node], S: Set[UndirectedGraph#Node],
                               D: Map[UndirectedGraph#Node, Int], P: Map[UndirectedGraph#Node, UndirectedGraph#Node]): Unit = {
    for (edge <- graph.edges if (edge.a == u || edge.b == u)) {
      var v = if (edge.a == u) edge.b else edge.a
      if (!S.contains(v)) {
        if (!D.contains(v) || D(v) > D(u) + edge.getWeight) {
          D(v) = D(u) + edge.getWeight
          P(v) = u
          Q += v
        }
      }
    }

  }
}

case class View(cells: String, _g: UndirectedGraph, myMap2: Map[String, UndirectedGraph#Node]) {
  val g = _g
  var myMap: Map[String, UndirectedGraph#Node] = myMap2

  val size = math.sqrt(cells.length).toInt
  val center = XY(size / 2, size / 2)

  val indexedCells = cells.view.zipWithIndex;

  def musu() = {
    val test = indexedCells.map(p => getXY(p._1, p._2))
    test
  }

  def getXY(c: Char, index: Int) = {
    var manoXY = relPosFromIndex(index)
    findRelatives(index, manoXY)
    manoXY
  }

  def findRelatives(index: Int, manoXY: XY) = {
    val Kairysis = if (index % size == 0) true else false
    val Desinysis = if ((index + 1) % size == 0) true else false
    val Virsutinis = if (index <= size - 1) true else false
    val Apatinis = if (cells.length - size <= index) true else false

    var n: UndirectedGraph#Node = null;
    if (myMap.contains(index.toString)) {
      n = myMap(index.toString)
    } else {
      n = g.addNode
      myMap += index.toString -> n
    }

    if (!Kairysis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index - 1).toString)) {
        n1 = myMap((index - 1).toString)
      } else {
        n1 = g.addNode
        myMap += (index - 1).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index - 1)))
    }
    if (!Desinysis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index + 1).toString)) {
        n1 = myMap((index + 1).toString)
      } else {
        n1 = g.addNode
        myMap += (index + 1).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index + 1)))
    }
    if (!Virsutinis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index - size).toString)) {
        n1 = myMap((index - size).toString)
      } else {
        n1 = g.addNode
        myMap += (index - size).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index - size)))
    }
    if (!Apatinis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index + size).toString)) {
        n1 = myMap((index + size).toString)
      } else {
        n1 = g.addNode
        myMap += (index + size).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index + size)))
    }
    if (!Kairysis && !Virsutinis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index - 1 - 31).toString)) {
        n1 = myMap((index - 1 - 31).toString)
      } else {
        n1 = g.addNode
        myMap += (index - 1 - 31).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index - 1 - 31)))
    }
    if (!Desinysis && !Virsutinis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index + 1 - 31).toString)) {
        n1 = myMap((index + 1 - 31).toString)
      } else {
        n1 = g.addNode
        myMap += (index + 1 - 31).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index + 1 - 31)))
    }
    if (!Kairysis && !Apatinis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index - 1 + 31).toString)) {
        n1 = myMap((index - 1 + 31).toString)
      } else {
        n1 = g.addNode
        myMap += (index - 1 + 31).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index - 1 + 31)))
    }
    if (!Desinysis && !Apatinis) {
      var n1: UndirectedGraph#Node = null
      if (myMap.contains((index + 1 + 31).toString)) {
        n1 = myMap((index + 1 + 31).toString)
      } else {
        n1 = g.addNode
        myMap += (index + 1 + 31).toString -> n1
      }
      n.connectWith(n1).setWeight(getWeightByChar(cells(index + 1 + 31)))
    }
  }

  def getWeightByChar(c: Char): Int = c match {
    case '_' => 4
    case '?' => 1000
    case 'W' => 10000
    case 'M' => 100 
    case 'm' => 750 //another master actuall 1000
    case 'S' => 6  
    case 's' => 500 //another slave
    case 'p' => 1000 //bad plant
    case 'P' => 2 //plant 
    case 'B' => 3 //beast
    case 'b' => 200 //bad beast actuall 400
    case _   => 10000
  }

  def offsetToNearest(c: Char) = {
    val relativePositions =
      cells
        .view
        .zipWithIndex
        .filter(_._1 == c)
        .map(p => relPosFromIndex(p._2))
    if (relativePositions.isEmpty)
      None
    else
      Some(relativePositions.minBy(_.length))
  }

  def apply(relPos: XY) = cellAtRelPos(relPos)

  def indexFromAbsPos(absPos: XY) = absPos.x + absPos.y * size
  def absPosFromIndex(index: Int) = XY(index % size, index / size)
  def absPosFromRelPos(relPos: XY) = relPos + center
  def cellAtAbsPos(absPos: XY) = cells.apply(indexFromAbsPos(absPos))

  def indexFromRelPos(relPos: XY) = indexFromAbsPos(absPosFromRelPos(relPos))
  def relPosFromAbsPos(absPos: XY) = absPos - center
  def relPosFromIndex(index: Int) = relPosFromAbsPos(absPosFromIndex(index))
  def cellAtRelPos(relPos: XY) = cells(indexFromRelPos(relPos))
}

case class XY(x: Int, y: Int) {

  override def toString() = {
    val rlvStr = relatives.mkString(", ")
    x + ":" + y + " " + rlvStr
  }

  var relatives = List[(Int, String)]()

  def isNonZero = x != 0 || y != 0
  def isZero = x == 0 && y == 0
  def isNonNegative = x >= 0 && y >= 0

  def updateX(newX: Int) = XY(newX, y)
  def updateY(newY: Int) = XY(x, newY)

  def addToX(dx: Int) = XY(x + dx, y)
  def addToY(dy: Int) = XY(x, y + dy)

  def +(pos: XY) = XY(x + pos.x, y + pos.y)
  def -(pos: XY) = XY(x - pos.x, y - pos.y)
  def *(factor: Double) = XY((x * factor).intValue, (y * factor).intValue)

  def distanceTo(pos: XY): Double = (this - pos).length
  def length: Double = math.sqrt(x * x + y * y)

  def signum = XY(x.signum, y.signum)

  def negate = XY(-x, -y)
  def negateX = XY(-x, y)
  def negateY = XY(x, -y)

}
}