istrinti([],[]).
istrinti([H], [H]).
istrinti([H,H|T], X) :-
    istrinti([H|T], X).
istrinti([H|T], [H|X]) :-
    istrinti(T, X).