﻿open System.IO
open System

[<EntryPoint>]
let main argv = 
    let filePath = "C:\Users\deile\Documents\Visual Studio 2015\Projects\deileo_lab3\deileo_lab3\input.txt"    
    let array = File.ReadLines(filePath)
    
    let DuomenuSarasas = array |> Seq.cast<string> |> Seq.toList    //Failo duomenys verciami i list'a

    let Masyvas = []
    printfn "%d"a
    let PridetiISarasa( Rezultatas: seq<int>, Sarasas: seq<int> ) =  //Kaupiamas atmusimu listas, 1-atmusta, 2-kitas bandymas, 3-baigiame programa
        let listas = Seq.append Sarasas Rezultatas 
        listas
        
    let PaimtiSkaiciu( index : int, DuomenuSarasas : seq<string> ) = Convert.ToInt32( array |> Seq.item index ) //paimti skaiciu is masyvo

    let rec Isvedimas( index : int, DuomenuSarasas : seq<int>, Atmusimai : int, session: int ) = 
        if DuomenuSarasas |> Seq.item index = 3 then ()
        else if DuomenuSarasas |> Seq.item index = 2 then
            System.Console.WriteLine("Test #{0}:", session+1)
            printfn "   maximum possible interceptions: %d" Atmusimai
            Isvedimas(index+1, DuomenuSarasas, 0, session+1)
        else if DuomenuSarasas |> Seq.item index = 1 then
            Isvedimas(index+1, DuomenuSarasas, Atmusimai+1, session)
        else ()
            
    
    let rec start (index : int, DuomenuSarasas : seq<string>, Atmustas : int, Masyvas : seq<int>) =
        let ilgis = DuomenuSarasas |> Seq.length
        if index < ilgis then
            let num = PaimtiSkaiciu (index, DuomenuSarasas)
            if num <> -1 && index = 0 then       //Ar pradinis skaicius
                start(index+1, DuomenuSarasas, num, PridetiISarasa([1], Masyvas))
            else if num <> -1 && num < Atmustas then        //Ar naujas skaicius didesnis uz senaji
                start(index+1, DuomenuSarasas, num, PridetiISarasa([1], Masyvas))
            else if num <> -1 && num > Atmustas then        //Ar naujas senasis skaicius didesnis uz naujaji
                start(index+1, DuomenuSarasas, Atmustas, Masyvas)
            else if num = -1 && PaimtiSkaiciu(index+1, DuomenuSarasas) = -1 then  //Ar testavimų pabaiga
                let l = PridetiISarasa([2], Masyvas)
                Isvedimas(0, PridetiISarasa([3], l), 0, 0)
            else                                                                // Ar vieno testavimo pabaiga
                let l1 = PridetiISarasa([2], Masyvas)
                start(index+2, DuomenuSarasas, PaimtiSkaiciu(index+1, DuomenuSarasas), PridetiISarasa([1], l1))
        else  
            ()
    
    start(0, DuomenuSarasas, 0, Masyvas) 
    System.Console.ReadKey() |> ignore
    printfn "%A" argv
    0